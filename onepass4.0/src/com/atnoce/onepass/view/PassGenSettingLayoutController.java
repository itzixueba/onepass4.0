/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.view;

import com.atnoce.onepass.Start;
import com.atnoce.onepass.core.BackService;
import com.atnoce.onepass.dao.OnepassConfigDao;
import com.atnoce.onepass.pojo.OnepassConfig;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.CommonUtil;
import com.atnoce.onepass.utils.QueueType;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author atnoce.com
 * @date 2016/11/6
 * @since 1.0.0
 */
public class PassGenSettingLayoutController {
    private Start start;
    private Stage stage;
    private final OnepassConfigDao cd=new OnepassConfigDao();
    @FXML
    private Slider daZiMu;
    @FXML
    private Slider xiaoZiMu;
    @FXML
    private Slider shuZi;
    @FXML
    private Slider fuHao;
    @FXML
    private Button saveConfig;
    @FXML
    private Button cancle;
    @FXML
    private Slider passLength;
    @FXML
    private CheckBox filterXingTong;
    public void setStart(Start start){
        this.start=start;
    }
    public void setPwdStage(Stage stage){
        this.stage=stage;
    }
    private OnepassConfig onepassConfig;
    @FXML
    private void initialize(){
        saveConfig.setOnAction(e->{
            saveConfigHandler();
        });
        cancle.setOnAction(e->{
            cancleHandler();
        });
        initUiData();
    }
    private void initUiData(){
        try {
            onepassConfig = cd.getOnepassConfig();
            if(onepassConfig!=null){
                daZiMu.setValue(Double.parseDouble(onepassConfig.getStrQuanZhong()));
                xiaoZiMu.setValue(Double.parseDouble(onepassConfig.getXiaoXieStrQuanZhong()));
                shuZi.setValue(Double.parseDouble(onepassConfig.getNumQuanZhong()));
                fuHao.setValue(Double.parseDouble(onepassConfig.getFuhaoQuanZhong()));
                passLength.setValue(Double.parseDouble(onepassConfig.getPassLength()));

                if(StringUtils.equals("true",onepassConfig.getFilterStr())){
                    filterXingTong.selectedProperty().setValue(true);
                }else{
                    filterXingTong.selectedProperty().setValue(false);
                }
            }
        } catch (SQLException e) {
            CommonUtil.alert(Alert.AlertType.ERROR,"配置数据初始化失败","初始化配置信息时发生系统错误!"+e.getMessage(),"错误").showAndWait();
        }catch(ClassNotFoundException e){
            CommonUtil.alert(Alert.AlertType.ERROR,"配置数据初始化失败","找不到数据库驱动!"+e.getMessage(),"错误").showAndWait();
        }
    }
    private void saveConfigHandler(){
        double d = daZiMu.getValue();
        double x = xiaoZiMu.getValue();
        double s = shuZi.getValue();
        double f = fuHao.getValue();
        double len=passLength.getValue();

        if(d+x+s+f<1){
            CommonUtil.alert(Alert.AlertType.ERROR,"生成器配置错误!","字母、数字、符号至少要有一个不为0才可以使用密码生成器!","错误").showAndWait();
            return;
        }
        if(len<1){
            CommonUtil.alert(Alert.AlertType.ERROR,"生成器配置错误!","密码长度至少要有1位!","错误").showAndWait();
            return;
        }
        onepassConfig.setStrQuanZhong(String.valueOf((int)d));
        onepassConfig.setXiaoXieStrQuanZhong(String.valueOf((int)x));
        onepassConfig.setNumQuanZhong(String.valueOf((int)s));
        onepassConfig.setFuhaoQuanZhong(String.valueOf((int)f));
        onepassConfig.setPassLength(String.valueOf((int)len));
        onepassConfig.setFilterStr(String.valueOf(filterXingTong.selectedProperty().getValue()));
        try {
            if(cd.savePassGenerConfig(onepassConfig)){
                Map<QueueType,String> queue=new HashMap<>();
                queue.put(QueueType.CONFIG,"");
                Cache.addUpdateModifyTimeQueue(queue);
                BackService.startUpdateModifyTimeService();

                CommonUtil.alert(Alert.AlertType.INFORMATION,null,"配置信息保存成功!","成功").showAndWait();
                stage.close();
            }
        } catch (SQLException e) {
            CommonUtil.alert(Alert.AlertType.ERROR,"保存失败","保存配置信息时发生系统错误!"+e.getMessage(),"错误").showAndWait();
        }catch(ClassNotFoundException e){
            CommonUtil.alert(Alert.AlertType.ERROR,"保存失败","找不到数据库驱动!"+e.getMessage(),"错误").showAndWait();
        }

    }
    private void cancleHandler(){

    }
}
