/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.pojo;

import com.atnoce.onepass.utils.CommonUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author atnoce.com
 * @date 2016/11/4 1:37
 * @since 1.0.0
 */
public class PasswordItem {
    private final StringProperty passId;
    private final StringProperty account;
    private final StringProperty passwordStr;
    private final StringProperty remarkStr;
    private final StringProperty createTime;
    private final StringProperty modifyTime;
    private final StringProperty isDelete;
    private final StringProperty dirTypeId;
    private final StringProperty sync;

    public PasswordItem(){
        passId=new SimpleStringProperty();
        account=new SimpleStringProperty();
        passwordStr=new SimpleStringProperty();
        remarkStr=new SimpleStringProperty();
        createTime=new SimpleStringProperty();
        modifyTime=new SimpleStringProperty();
        isDelete=new SimpleStringProperty();
        dirTypeId=new SimpleStringProperty();
        sync=new SimpleStringProperty();
    }

    public PasswordItem(String account,String passwordStr,String remarkStr,String dirTypeId){
        this.passId=new SimpleStringProperty(CommonUtil.getId());
        this.account=new SimpleStringProperty(account);
        this.passwordStr=new SimpleStringProperty(passwordStr);
        this.remarkStr=new SimpleStringProperty(remarkStr);
        this.createTime=new SimpleStringProperty();
        isDelete=new SimpleStringProperty("false");
        this.dirTypeId=new SimpleStringProperty(dirTypeId);
        this.sync=new SimpleStringProperty(System.currentTimeMillis()+"");
        this.modifyTime=new SimpleStringProperty(System.currentTimeMillis()+"");
    }

    /**
     *
     * @param passId 密码项ID
     * @param account 账号
     * @param passwordStr 密码字符串
     * @param remarkStr 备注
     * @param dirTypeId 目录ID
     * @param createTime 创建时间
     * @param modifyTime 修改时间
     * @param isDelete 是否删除
     * @param sync 是否同步
     */
    public PasswordItem(String passId,String account,String passwordStr,String remarkStr,String dirTypeId,String createTime,
                        String modifyTime,String isDelete,String sync){
        this.passId=new SimpleStringProperty(passId);
        this.account=new SimpleStringProperty(account);
        this.passwordStr=new SimpleStringProperty(passwordStr);
        this.remarkStr=new SimpleStringProperty(remarkStr);
        this.createTime=new SimpleStringProperty(createTime);
        this.modifyTime=new SimpleStringProperty(modifyTime);
        this.isDelete=new SimpleStringProperty(isDelete);
        this.dirTypeId=new SimpleStringProperty(dirTypeId);
        this.sync=new SimpleStringProperty(sync);
    }

    public  StringProperty passIdProperty() {
        return this.passId;
    }

    public  String getPassId() {
        return this.passIdProperty().get();
    }

    public  void setPassId( String passId) {
        this.passIdProperty().set(passId);
    }

    public  StringProperty accountProperty() {
        return this.account;
    }

    public  String getAccount() {
        return this.accountProperty().get();
    }

    public  void setAccount(String account) {
        this.accountProperty().set(account);
    }

    public  StringProperty passwordStrProperty() {
        return this.passwordStr;
    }

    public  String getPasswordStr() {
        return this.passwordStrProperty().get();
    }

    public  void setPasswordStr( String passwordStr) {
        this.passwordStrProperty().set(passwordStr);
    }

    public  StringProperty remarkStrProperty() {
        return this.remarkStr;
    }

    public  String getRemarkStr() {
        return this.remarkStrProperty().get();
    }

    public  void setRemarkStr( String remarkStr) {
        this.remarkStrProperty().set(remarkStr);
    }

    public StringProperty createTimeProperty() {
        return this.createTime;
    }

    public  String getCreateTime() {
        return this.createTimeProperty().get();
    }

    public  void setCreateTime( String createTime) {
        this.createTimeProperty().set(createTime);
    }

    public  StringProperty isDeleteProperty() {
        return this.isDelete;
    }

    public  String getIsDelete() {
        return this.isDeleteProperty().get();
    }

    public  void setIsDelete( String isDelete) {
        this.isDeleteProperty().set(isDelete);
    }

    public  StringProperty dirTypeIdProperty() {
        return this.dirTypeId;
    }

    public  String getDirTypeId() {
        return this.dirTypeIdProperty().get();
    }

    public  void setDirTypeId( String dirTypeId) {
        this.dirTypeIdProperty().set(dirTypeId);
    }
    public String getSync(){
        return this.sync.get();
    }
    public void setSync(String sync){
        this.syncProperty().set(sync);
    }
    public StringProperty syncProperty(){
        return sync;
    }
    public String getModifyTime(){
        return this.modifyTime.get();
    }
    public void setModifyTime(String modifyTime){
        this.modifyTime.set(modifyTime);
    }
    public StringProperty modifyTimeProperty(){
        return modifyTime;
    }
}
