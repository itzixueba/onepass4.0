/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.core;

import com.atnoce.onepass.pojo.OnepassConfig;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * @author atnoce.com
 * @date 2016/11/6
 * @since 1.0.0
 */
public class CoreUtil {
    private static final String[] DAXIE_ZIMU={"A","B","X","Y","C","F","G","H","I","J","K","L","M","N"
            ,"O","P","Q","R","S","T","D","E","U","V","W","Z"};
    private static final String[] XIAOXIE_ZIMU={"a","d","e","f","g","h","k","l","m","n",
            "o","p","i","j","q","r","s","t","u","v","w","x","b","c","y","z"};
    private static final String[] NUMBER={"0","7","1","6","2","3","5","8","9","4"};
    private static final String[] FUHAO={",",".","?","<",">","#","!","@","$","%","&","^","'","*","(",
            ")","-","_","+","=","/","\\"};
    public static final String getId(){
        return UUID.randomUUID().toString();
    }
    public static final String getGenerPassStr(OnepassConfig oc){
        List list=autoGenerPass(
                Integer.parseInt(oc.getStrQuanZhong()),
                Integer.parseInt(oc.getXiaoXieStrQuanZhong()),
                Integer.parseInt(oc.getNumQuanZhong()),
                Integer.parseInt(oc.getFuhaoQuanZhong()));
        if(StringUtils.equals("true",oc.getFilterStr())){
            String[] filter={"O","0","o","l","1"};
            list.removeAll(Arrays.asList(filter));
        }
        int passLen=Integer.parseInt(oc.getPassLength());
        int max=list.size();
        int min=0;
        Random random = new Random();
        StringBuffer pass=new StringBuffer();
        for(int a=0;a<passLen;a++){
            pass.append(list.get(random.nextInt(max)%(max-min+1) + min));

        }

        return pass.toString();
    }
    private static List autoGenerPass(int zimu, int zim_x, int number, int fuhao){
        List list=new ArrayList();
        for(int a=0;a<zimu;a++){
            list.addAll(Arrays.asList(DAXIE_ZIMU));
        }
        for(int a=0;a<zim_x;a++){
            list.addAll(Arrays.asList(XIAOXIE_ZIMU));
        }
        for(int a=0;a<number;a++){
            list.addAll(Arrays.asList(NUMBER));
        }
        for(int a=0;a<fuhao;a++){
            list.addAll(Arrays.asList(FUHAO));
        }
        return list;
    }
}
