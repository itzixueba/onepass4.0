/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;
import org.apache.commons.lang.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @author atnoce.com
 * @date 2016/11/4 1:38
 * @since 1.0.0
 */
public class CommonUtil {
    public static final String APP_NAME="onepass";
    public static final String APP_VERSION="4.1.1";
    public static final String BUILD_TIME="2017-01-01";
    public final static String COLUD_ACCOUNT_KEY="BC99ED5371B5162EFC9D071B5215F735907026A2";
    public final static int CONNECTION_TIMEOUT=5000;
    public static final String getId(){
        return UUID.randomUUID().toString();
    }
    public static DateTimeFormatter getDateTimeFormatter(){
        return DateTimeFormatter.ofPattern("uuuu-MM-dd");
    }
    public static Tooltip getTooltip(String context, int size){
        Tooltip tip=new Tooltip();
        tip.setText(context);
        tip.setFont(new Font("Arial", size));
        return tip;
    }
    public static String getTime(){
        return System.currentTimeMillis()+"";
    }
   /* public static void updateSyncState(){
        try {
            InitContext.init();//刷新配置信息
            *//*if(!Boolean.valueOf((String)Cache.proMap.get("localupdate"))){
                long time = BasicNetService.getTime();
                InitContext.updateProperties("localupdate", "true");
                InitContext.updateProperties("localtime", time+"");
            }*//*
        } catch (IOException ex) {
        }
    }*/
    /**
     * 检查账号格式<br>
     *     1.长度不能超过100位<br>
     * @param account
     * @return
     */
    public static String checkCloudAccount(String account){
        String result="";
        if(StringUtils.isEmpty(account)){
            result="云账号不能为空!";
        }else if(account.length()>100){
            result="云账号长度不能超过100个字符!";
        }

        return result;
    }

    /**
     * 创建alert提示框
     * @param type
     * @param header
     * @param contentText
     * @param title
     * @return
     */
    public static Alert alert(Alert.AlertType type,String header,String contentText,String title){
        Alert alert=new Alert(type);
        alert.setHeaderText(header);
        alert.setContentText(contentText);
        alert.setTitle(title);
        return alert;
    }
}
