package com.atnoce.onepass.utils;

/**
 * @author atnoce.com
 * @date 2016/11/4 0:23
 * @since 1.0.0
 */
public enum Const {
    /**
     * 数据库路径
     */
    DB_PATH,
    /**
     * 是否激活云服务
     */
    ACTIONCLOUD,

    /**
     * 是否第一次启动应用
     */
    FIRSTSTART,
    /**
     * 云服务账号
     */
    COLUDACCOUNT,
    /**
     * 云服务地址
     */
    BASE_SERVER_ADDR,
    /**
     * 授时服务器地址
     */
    TIMESERVER
}
