package com.atnoce.onepass;
/**
 * @author atnoce.com
 * @date 2016/11/3 23:58
 * @since 1.0.0
 */

import com.atnoce.onepass.core.BackService;
import com.atnoce.onepass.pojo.PasswordItem;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.CommonUtil;
import com.atnoce.onepass.utils.Const;
import com.atnoce.onepass.utils.InitContext;
import com.atnoce.onepass.view.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.EventListener;

public class Start extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Stage stage;
    private BorderPane rootPane;
    /**
     * 启动
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        stage=primaryStage;
        stage.setTitle(CommonUtil.APP_NAME);

        if (StringUtils.equals(Cache.getProConfig(Const.FIRSTSTART),"true")){
            //第一次启动客户端
            BeforeLoginLayout();
        }else{
            initLoginLayout(false);
        }
        addRefshLockTime(stage);
    }

    private void BeforeLoginLayout() {
        try{
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/BeforeLogin.fxml"));
            rootPane=(BorderPane)loader.load();
            Scene scene=new Scene(rootPane);
            stage.setScene(scene);
            stage.show();

            BeforeLoginController controller=loader.getController();
            controller.setStart(Start.this);
        } catch (IOException ex) {
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("启动失败");
            alert.setContentText("加载BeforeLogin.fxml文件发生异常!");
            alert.showAndWait();
            Platform.exit();
        }
    }

    /**
     * 初始化登录布局界面
     * @param synch
     */
    public void initLoginLayout(boolean synch) {
        try {
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/LoginLayout.fxml"));
            BorderPane loginPanel=loader.load();

            LoginLayoutController controller=loader.getController();
            controller.setStart(Start.this);

            Scene scene=new Scene(loginPanel);
            stage.setScene(scene);
            stage.resizableProperty().setValue(false);
            stage.show();

            /*if (synch){
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        BackService.startSyncService(null);
                    }
                });
            }*/


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化主页面
     */
    public void initIndexLayout(){
        try{
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/IndexLayout.fxml"));
            rootPane=loader.load();

            Scene scene=new Scene(rootPane);
            stage.setScene(scene);
            stage.resizableProperty().setValue(Boolean.TRUE);
            IndexLayoutController controller=loader.getController();
            controller.setStart(Start.this);
            if (StringUtils.equals("true",Cache.getOc().getKongXianLock())){
                controller.startKongXianLock();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化系统配置项目
     */
    @Override
    public void init(){
        try {
            //初始化配置信息到系统中
            InitContext.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showPassEditDialog(PasswordItem pi) {
        try{
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/PasswordItemEdit.fxml"));
            AnchorPane page=loader.load();

            Stage dialogStage=new Stage();
            dialogStage.setTitle("编辑密码");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            //dialogStage.initStyle(StageStyle.UNDECORATED);
            Scene scene=new Scene(page);
            dialogStage.setScene(scene);

            addRefshLockTime(dialogStage);

            PasswordItemEditController controller=loader.getController();
            controller.setStart(Start.this);
            controller.setDialogStage(dialogStage);
            controller.setPdi(pi);

            dialogStage.showAndWait();
            return controller.isOkClicked();
        }catch(IOException e){
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("布局文件加载失败");
            alert.setContentText("加载PasswordItemEdit.fxml文件发生异常!");
            alert.showAndWait();
            return false;
        }
    }

    public void showPwdGenSettingDialog() {
        try{
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/PassGenSettingLayout.fxml"));
            BorderPane panel=loader.load();

            Stage genSettingStage=new Stage();
            genSettingStage.setTitle("密码生成器配置");
            genSettingStage.initModality(Modality.WINDOW_MODAL);
            genSettingStage.initOwner(stage);
            Scene scene=new Scene(panel);
            genSettingStage.setScene(scene);

            addRefshLockTime(genSettingStage);

            PassGenSettingLayoutController controller=loader.getController();
            controller.setStart(Start.this);
            controller.setPwdStage(genSettingStage);
            genSettingStage.showAndWait();
        }catch(IOException e){
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("布局文件加载失败");
            alert.setContentText("加载PassGenSettingLayout.fxml文件发生异常!");
            alert.show();
        }
    }

    public void showPreferencesDialog() {
        try{
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(Start.class.getResource("view/PreferencesLayout.fxml"));
            BorderPane bp=loader.load();

            Stage preferenceStage=new Stage();
            preferenceStage.setTitle("系统设置");
            preferenceStage.initModality(Modality.WINDOW_MODAL);
            preferenceStage.initOwner(stage);
            Scene scene=new Scene(bp);
            preferenceStage.setScene(scene);

            addRefshLockTime(preferenceStage);


            PreferencesLayoutController controller=loader.getController();
            controller.setStart(Start.this);
            controller.setPreferencesDialogStage(preferenceStage);

            preferenceStage.showAndWait();


        }catch(IOException e){
            e.printStackTrace();
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("布局文件加载失败");
            alert.setContentText("加载PreferencesLayout.fxml文件发生异常!");
            alert.show();
        }
    }
    private void addRefshLockTime(Stage srcStage){
        srcStage.addEventFilter(EventType.ROOT, new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                if (StringUtils.equals("MOUSE_CLICKED",event.getEventType().getName())){
                    BackService.updateClock();
                }
            }
        });
    }
    public Stage getStage(){
        return stage;
    }
}
